# Java OpenJdk 11.0.7 Docker image: https://hub.docker.com/layers/openjdk/library/openjdk/11.0.7-jdk/images/sha256-29fc9543dead0f8c1b25afa026e4a3ddafa38c5c353ee407edcea62a95f1f2e4?context=explore
FROM openjdk@sha256:29fc9543dead0f8c1b25afa026e4a3ddafa38c5c353ee407edcea62a95f1f2e4
RUN useradd -ms /bin/bash appuser
WORKDIR /home/appuser


COPY ./target/shakespearean-pokemon-api-*.jar shakespearean-pokemon-api.jar
COPY ./src/main/resources/log4j2.xml log4j2.xml

EXPOSE 8080

RUN chown -R appuser:appuser  /home/appuser/
USER appuser
CMD java -jar -Dlogging.config=/home/appuser/log4j2.xml shakespearean-pokemon-api.jar
