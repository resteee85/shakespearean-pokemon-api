package com.acme.shakespeareanpokemon.dto

data class ShakespeaereTranslationResponseDTO(private val success: Map<String, Any>?, private val contents: Map<String, String>?) {

    companion object {
        const val TRANSLATED_FIELD = "translated"
    }

    fun getTranslatedText(): String? = contents?.get(TRANSLATED_FIELD)

}