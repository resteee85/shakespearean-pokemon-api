package com.acme.shakespeareanpokemon.dto

/**
 * Rest controller response
 */
data class ShakespeareanPokemonDTO(val name: String,
                                   val description: String)