package com.acme.shakespeareanpokemon.service

import com.acme.shakespeareanpokemon.utils.NumbersUtils
import com.fasterxml.jackson.databind.JsonNode
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.util.concurrent.CompletableFuture

/**
 * This service aims to retrieve one of the pokemon descriptions (flavor_text) calling several services
 */
@Service
class PokemonServiceImpl : PokemonService {

    private val logger = LoggerFactory.getLogger(PokemonServiceImpl::class.java)


    @Autowired
    private lateinit var restTemplate: RestTemplate

    @Value("\${api.pokemon.url}")
    private lateinit var pokemonApiUrl: String

    companion object {
        const val SPECIES_FIELD = "species"
        const val URL_FIELD = "url"
        const val FLAVOR_TEXT_ENTRIES_FIELD = "flavor_text_entries"
        const val LANGUAGE_FIELD = "language"
        const val EN_LANGUAGE_VALUE = "en"
        const val NAME_FIELD = "name"
        const val FLAVOR_TEXT_FIELD = "flavor_text"
    }


    @Async
    override fun getPokemonDescriptionByName(name: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            logger.info("Retrieving pokemon {$name}")
            getPokemonData(name)
        }.thenApply {
            logger.trace("Retrieved the following data for pokemon {$name}: {$it}")
            it?.get(SPECIES_FIELD)?.get(URL_FIELD)?.asText() // Retrieving URL field of species json array
        }.thenCompose {
            CompletableFuture.supplyAsync {
                logger.info("Retrieving species for pokemon {$name}")
                logger.trace("Species URL: $it")
                getPokemonSpecies(it)
            }
        }.thenApply {
            logger.info("Extracting random text for pokemon {$name}")
            logger.trace("Retrieved flavor_text for pokemon {$name}: $it")
            extractRandomFlavorText(it?.get(FLAVOR_TEXT_ENTRIES_FIELD))
        }

    }


    /**
     * This method calls the <pokemonApiUrl>/pokemonName url in order to obtain pokemon general data
     */
    private fun getPokemonData(name: String): JsonNode? {
        return this.restTemplate.getForEntity("$pokemonApiUrl/$name", JsonNode::class.java).body
    }

    /**
     * This method calls the species endpoint in order to obtain the species description and flavor_textes
     * We need to call this services since the descriptions of the pokemon are not present in the first call!!!
     */
    private fun getPokemonSpecies(speciesUrl: String?): JsonNode? {
        return speciesUrl?.let {
            this.restTemplate.getForEntity(speciesUrl, JsonNode::class.java).body
        }
    }

    /**
     * Picks from the flavor_text_entries array a random text and returns it. If the flavor_text_entries isn't found it
     * returns empty string
     */
    private fun extractRandomFlavorText(entries: JsonNode?): String? {
        return entries?.let {
            if (it.isArray && it.size() > 0) {
                val engFlavorTextEntries = it.filter { it?.get(LANGUAGE_FIELD)?.get(NAME_FIELD)?.asText() == EN_LANGUAGE_VALUE }
                return@let engFlavorTextEntries?.get(NumbersUtils.randomBetween(0, engFlavorTextEntries.size - 1))?.get(FLAVOR_TEXT_FIELD)?.asText()
            } else ""
        }
    }

}


