package com.acme.shakespeareanpokemon.service

import com.acme.shakespeareanpokemon.dto.ShakespeaereTranslationResponseDTO
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.net.URLEncoder
import java.nio.charset.Charset
import java.util.concurrent.CompletableFuture

/**
 * This service aims to translate a text into Shakespearean terms
 */
@Service
class ShakespeareServiceImpl : ShakespeareService {

    private val logger = LoggerFactory.getLogger(ShakespeareServiceImpl::class.java)

    @Autowired
    private lateinit var restTemplate: RestTemplate

    @Value("\${api.shakespeare.url}")
    private lateinit var shakespeareApiUrl: String

    override fun getShakespeareTranslation(text: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            logger.info("Translating the following text: {$text}")
            translate(text)
        }
    }

    /**
     * This method calls the Shakespearean translation service in order to get the translation
     */
    private fun translate(text: String): String {
        val encodedText = URLEncoder.encode(text, Charset.forName("UTF-8"))
        val response = this.restTemplate.postForEntity("$shakespeareApiUrl?text=$encodedText", null, ShakespeaereTranslationResponseDTO::class.java)
        return response.body?.getTranslatedText() ?: ""
    }

}