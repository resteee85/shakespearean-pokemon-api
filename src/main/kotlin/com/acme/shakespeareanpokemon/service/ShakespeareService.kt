package com.acme.shakespeareanpokemon.service

import java.util.concurrent.CompletableFuture

interface ShakespeareService {

    fun getShakespeareTranslation(text: String): CompletableFuture<String>

}