package com.acme.shakespeareanpokemon.service

import java.util.concurrent.CompletableFuture

interface PokemonService {

    fun getPokemonDescriptionByName(name: String): CompletableFuture<String>

}