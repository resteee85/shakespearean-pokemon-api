package com.acme.shakespeareanpokemon.config

import com.acme.shakespeareanpokemon.handler.RestTemplateResponseErrorHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class RestTemplateConfig {

    @Bean
    fun restTemplate(): RestTemplate {
        val restTemplate = RestTemplate()
        restTemplate.errorHandler = RestTemplateResponseErrorHandler()
        return restTemplate
    }

}