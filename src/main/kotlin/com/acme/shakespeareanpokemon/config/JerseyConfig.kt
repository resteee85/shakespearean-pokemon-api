package com.acme.shakespeareanpokemon.config

import com.acme.shakespeareanpokemon.rest.ShakespeareanPokemonControllerImpl
import org.glassfish.jersey.server.ResourceConfig
import org.springframework.stereotype.Component


@Component
class JerseyConfig : ResourceConfig() {
    init {
        registerEndpoints()
    }

    private fun registerEndpoints() {
        register(ShakespeareanPokemonControllerImpl::class.java)
    }
}