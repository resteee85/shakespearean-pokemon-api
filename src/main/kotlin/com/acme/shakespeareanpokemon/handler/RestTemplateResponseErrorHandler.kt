package com.acme.shakespeareanpokemon.handler

import org.springframework.http.HttpStatus
import org.springframework.http.client.ClientHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.client.ResponseErrorHandler
import java.io.IOException
import javax.ws.rs.ForbiddenException
import javax.ws.rs.InternalServerErrorException
import javax.ws.rs.NotAuthorizedException
import javax.ws.rs.NotFoundException
import kotlin.jvm.Throws


@Component
class RestTemplateResponseErrorHandler : ResponseErrorHandler {

    @Throws(IOException::class)
    override fun hasError(httpResponse: ClientHttpResponse): Boolean {
        return httpResponse.getStatusCode().series() === HttpStatus.Series.CLIENT_ERROR || httpResponse.getStatusCode().series() === HttpStatus.Series.SERVER_ERROR
    }

    @Throws(IOException::class)
    override fun handleError(httpResponse: ClientHttpResponse) {
        if (httpResponse.getStatusCode()
                        .series() === HttpStatus.Series.SERVER_ERROR) {
            throw InternalServerErrorException()
        } else if (httpResponse.getStatusCode()
                        .series() === HttpStatus.Series.CLIENT_ERROR) {
            when (httpResponse.statusCode) {
                HttpStatus.NOT_FOUND -> throw NotFoundException()
                HttpStatus.UNAUTHORIZED -> throw NotAuthorizedException("User not authorized")
                HttpStatus.FORBIDDEN -> throw ForbiddenException()
                else -> HttpStatus.BAD_REQUEST
            }
        }
    }
}