package com.acme.shakespeareanpokemon

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShakespeareanPokemonApiApplication

fun main(args: Array<String>) {
    runApplication<ShakespeareanPokemonApiApplication>(*args)
}
