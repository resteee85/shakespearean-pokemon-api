package com.acme.shakespeareanpokemon.rest

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.container.AsyncResponse
import javax.ws.rs.container.Suspended
import javax.ws.rs.core.MediaType

interface ShakespeareanPokemonController {

    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getPokemon(
            @PathParam("name") name: String,
            @Suspended asyncResponse: AsyncResponse
    )

}