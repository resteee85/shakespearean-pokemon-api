package com.acme.shakespeareanpokemon.rest

import com.acme.shakespeareanpokemon.dto.ShakespeareanPokemonDTO
import com.acme.shakespeareanpokemon.service.PokemonService
import com.acme.shakespeareanpokemon.service.ShakespeareService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.net.URLDecoder
import java.nio.charset.Charset
import javax.ws.rs.Path
import javax.ws.rs.container.AsyncResponse

@Component
@Path("pokemon")
class ShakespeareanPokemonControllerImpl : ShakespeareanPokemonController {

    private val logger = LoggerFactory.getLogger(ShakespeareanPokemonControllerImpl::class.java)

    @Autowired
    private lateinit var pokemonService: PokemonService

    @Autowired
    private lateinit var shakespeareService: ShakespeareService

    override fun getPokemon(name: String, asyncResponse: AsyncResponse) {
        logger.info("Translation for pokemon {$name} requested!")
        pokemonService
                .getPokemonDescriptionByName(name)
                .thenCompose {
                    shakespeareService.getShakespeareTranslation(it)
                }
                .handle { value, exception ->
                    if (exception != null) {
                        asyncResponse.resume(exception)
                    } else {
                        asyncResponse.resume(ShakespeareanPokemonDTO(name, URLDecoder.decode(value, Charset.forName("UTF-8"))))
                    }
                }

    }


}