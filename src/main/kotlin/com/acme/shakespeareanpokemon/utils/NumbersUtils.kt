package com.acme.shakespeareanpokemon.utils

class NumbersUtils {

    companion object {
        fun randomBetween(min: Int, max: Int) = (min..max).random()
    }

}