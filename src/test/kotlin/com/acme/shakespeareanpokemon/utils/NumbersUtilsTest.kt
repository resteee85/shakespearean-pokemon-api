package com.acme.shakespeareanpokemon.utils

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class NumbersUtilsTest {

    @Test
    fun testRandomNumber() {
        val rand = NumbersUtils.randomBetween(0, 5)
        Assert.assertTrue(rand in 0..5)
    }

    @Test
    fun testSameMinMax() {
        val rand = NumbersUtils.randomBetween(0, 0)
        Assert.assertTrue(rand in 0..0)
        Assert.assertTrue(rand == 0)
    }

    @Test(expected = NoSuchElementException::class)
    fun testMinGreaterThanMax() {
        NumbersUtils.randomBetween(0, -1)
    }


}