package com.acme.shakespeareanpokemon

import com.acme.shakespeareanpokemon.dto.ShakespeaereTranslationResponseDTO
import com.fasterxml.jackson.databind.JsonNode
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

abstract class AbstractServiceTest {

    fun generateMockResponseForPokemonService(json: JsonNode, httpStatus: HttpStatus): ResponseEntity<JsonNode> {
        return ResponseEntity(json, httpStatus)
    }

    fun generateMockResponseForShakespeareService(response: ShakespeaereTranslationResponseDTO, httpStatus: HttpStatus): ResponseEntity<ShakespeaereTranslationResponseDTO> {
        return ResponseEntity(response, httpStatus)
    }

}