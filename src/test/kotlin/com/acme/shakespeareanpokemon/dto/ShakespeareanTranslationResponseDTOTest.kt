package com.acme.shakespeareanpokemon.dto

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class ShakespeareanTranslationResponseDTOTest {

    @Test
    fun testGetTranslatedTextFromDtoWithContents() {
        val translated = "imtranslated"
        val contents = mutableMapOf<String, String>()
        contents.put("translated", translated)
        val response = ShakespeaereTranslationResponseDTO(emptyMap(), contents)
        Assert.assertEquals(translated, response.getTranslatedText()!!)
    }

    @Test
    fun testGetTranslatedTextFromDtoWithoutContents() {
        val contents = mutableMapOf<String, String>()
        val response = ShakespeaereTranslationResponseDTO(emptyMap(), contents)
        Assert.assertTrue(response.getTranslatedText() == null)
    }

    @Test
    fun testGetTranslatedTextFromDtoWithNullContents() {
        val response = ShakespeaereTranslationResponseDTO(emptyMap(), null)
        Assert.assertTrue(response.getTranslatedText() == null)
    }

}