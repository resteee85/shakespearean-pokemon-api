package com.acme.shakespeareanpokemon.rest

import com.acme.shakespeareanpokemon.AbstractServiceTest
import com.acme.shakespeareanpokemon.dto.ShakespeaereTranslationResponseDTO
import com.acme.shakespeareanpokemon.dto.ShakespeareanPokemonDTO
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import javax.ws.rs.BadRequestException
import javax.ws.rs.NotFoundException

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ShakespeareanPokemonControllerTest : AbstractServiceTest() {

    @LocalServerPort
    private lateinit var port: Integer

    @MockBean
    private val mockRestTemplate: RestTemplate? = null


    private val restTemplate: RestTemplate = RestTemplate()

    @Value("\${api.pokemon.url}")
    private lateinit var pokemonApiUrl: String

    @Test
    fun testCompleteChainOK() {
        // Mocking calls to Pokemon Service
        val firstServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_response.json"), JsonNode::class.java)
        val secondServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_species_response.json"), JsonNode::class.java)
        val pokemonName = "pikachu"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"
        val secondServiceUrl = "https://fake/api/v2/pokemon-species/25/"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(firstServiceResponse, HttpStatus.OK))

        `when`(mockRestTemplate?.getForEntity(eq(secondServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(secondServiceResponse, HttpStatus.OK))


        // Mocking calls to Shakespeare Service
        val translatedTxt = "translated text"
        val originalTxt = "original text"
        val successField = mutableMapOf<String, Any>()
        successField.put("total", 1)
        val contentsField = mutableMapOf<String, String>()
        contentsField.put("translated", translatedTxt)
        contentsField.put("text", originalTxt)
        contentsField.put("translation", "shakespeare")
        val expected = ShakespeaereTranslationResponseDTO(successField, contentsField)
        val response = generateMockResponseForShakespeareService(expected, HttpStatus.OK)
        `when`(mockRestTemplate?.postForEntity(ArgumentMatchers.anyString(), ArgumentMatchers.any(), eq(ShakespeaereTranslationResponseDTO::class.java))).thenReturn(response)

        val translationResponse = getShakespeareanPokemon(pokemonName)


        Assert.assertTrue(translationResponse.statusCode == HttpStatus.OK)
        Assert.assertEquals(translatedTxt, translationResponse.body!!.description)
        Assert.assertEquals(pokemonName, translationResponse.body!!.name)
    }

    @Test(expected = HttpClientErrorException.NotFound::class)
    fun testPokemonServiceNotFoundException() {
        // Mocking calls to Pokemon Service
        val pokemonName = "nonExistingPokemon"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenThrow(NotFoundException())

        getShakespeareanPokemon(pokemonName)

    }

    @Test(expected = HttpClientErrorException.BadRequest::class)
    fun testPokemonServiceBadReqException() {
        // Mocking calls to Pokemon Service
        val pokemonName = "nonExistingPokemon"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenThrow(BadRequestException())

        getShakespeareanPokemon(pokemonName)

    }

    @Test(expected = HttpServerErrorException.InternalServerError::class)
    fun testPokemonServiceRuntimeException() {
        // Mocking calls to Pokemon Service
        val pokemonName = "nonExistingPokemon"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenThrow(RuntimeException())

        getShakespeareanPokemon(pokemonName)

    }

    @Test(expected = HttpServerErrorException.InternalServerError::class)
    fun testShakespeareTranslationServiceRuntimeException() {
        // Mocking calls to Pokemon Service
        val firstServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_response.json"), JsonNode::class.java)
        val secondServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_species_response.json"), JsonNode::class.java)
        val pokemonName = "pikachu"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"
        val secondServiceUrl = "https://fake/api/v2/pokemon-species/25/"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(firstServiceResponse, HttpStatus.OK))

        `when`(mockRestTemplate?.getForEntity(eq(secondServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(secondServiceResponse, HttpStatus.OK))


        // Mocking calls to Shakespeare Service
        `when`(mockRestTemplate?.postForEntity(ArgumentMatchers.anyString(), ArgumentMatchers.any(), eq(ShakespeaereTranslationResponseDTO::class.java)))
                .thenThrow(RuntimeException())

        getShakespeareanPokemon(pokemonName)
    }

    fun getShakespeareanPokemon(pokemonName: String): ResponseEntity<ShakespeareanPokemonDTO> {
        return restTemplate.getForEntity("http://localhost:$port/pokemon/$pokemonName", ShakespeareanPokemonDTO::class.java)
    }


}