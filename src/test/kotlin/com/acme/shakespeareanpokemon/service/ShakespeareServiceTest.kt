package com.acme.shakespeareanpokemon.service

import com.acme.shakespeareanpokemon.AbstractServiceTest
import com.acme.shakespeareanpokemon.dto.ShakespeaereTranslationResponseDTO
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.RestTemplate


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ShakespeareServiceTest : AbstractServiceTest() {

    @MockBean
    private val mockRestTemplate: RestTemplate? = null // mocks the restTemplate inside ShakespeareService

    @Autowired
    private lateinit var shakespeareService: ShakespeareService

    @Test
    fun testServiceWithOKResponse() {
        // Generating mock response of shakespeare translation service
        val translatedTxt = "translated text"
        val originalTxt = "original text"
        val successField = mutableMapOf<String, Any>()
        successField.put("total", 1)
        val contentsField = mutableMapOf<String, String>()
        contentsField.put("translated", translatedTxt)
        contentsField.put("text", originalTxt)
        contentsField.put("translation", "shakespeare")
        val expected = ShakespeaereTranslationResponseDTO(successField, contentsField)
        val response = generateMockResponseForShakespeareService(expected, HttpStatus.OK)
        `when`(mockRestTemplate?.postForEntity(anyString(), any(), eq(ShakespeaereTranslationResponseDTO::class.java))).thenReturn(response)

        val result = shakespeareService.getShakespeareTranslation(originalTxt).get()
        Assert.assertEquals(translatedTxt, result)
    }

    @Test
    fun testServiceWithEmptyResponse() {
        // Generating mock response of shakespeare translation service
        val successField = mutableMapOf<String, Any>()
        val contentsField = mutableMapOf<String, String>()
        val expected = ShakespeaereTranslationResponseDTO(successField, contentsField)
        val response = generateMockResponseForShakespeareService(expected, HttpStatus.OK)
        `when`(mockRestTemplate?.postForEntity(anyString(), any(), eq(ShakespeaereTranslationResponseDTO::class.java))).thenReturn(response)

        val result = shakespeareService.getShakespeareTranslation("testtest").get()
        Assert.assertEquals("", result)
    }


}