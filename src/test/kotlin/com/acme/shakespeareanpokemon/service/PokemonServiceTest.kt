package com.acme.shakespeareanpokemon.service

import com.acme.shakespeareanpokemon.AbstractServiceTest
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.RestTemplate

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PokemonServiceTest : AbstractServiceTest() {

    @MockBean
    private val mockRestTemplate: RestTemplate? = null // mocks the restTemplate inside PokemonService

    @Autowired
    private lateinit var pokemonService: PokemonService

    @Value("\${api.pokemon.url}")
    private lateinit var pokemonApiUrl: String


    @Test
    fun testWithAllResponsesOK() {

        val firstServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_response.json"), JsonNode::class.java)
        val secondServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_species_response.json"), JsonNode::class.java)
        val pokemonName = "pikachu"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"
        val secondServiceUrl = "https://fake/api/v2/pokemon-species/25/"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(firstServiceResponse, HttpStatus.OK))

        `when`(mockRestTemplate?.getForEntity(eq(secondServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(secondServiceResponse, HttpStatus.OK))

        val response = pokemonService.getPokemonDescriptionByName(pokemonName).get()
        Assert.assertTrue(response != null)
        Assert.assertTrue(response.isNotBlank())

    }

    @Test
    fun testWithFirstResponseEmpty() {

        val firstServiceResponse = ObjectMapper().readValue("{}", JsonNode::class.java)
        val secondServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_species_response.json"), JsonNode::class.java)
        val pokemonName = "pikachu"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"
        val secondServiceUrl = "https://fake/api/v2/pokemon-species/25/"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(firstServiceResponse, HttpStatus.OK))

        `when`(mockRestTemplate?.getForEntity(eq(secondServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(secondServiceResponse, HttpStatus.OK))

        val response = pokemonService.getPokemonDescriptionByName(pokemonName).get()
        Assert.assertTrue(response == null)

    }

    @Test
    fun testWithSecondResponseEmpty() {

        val firstServiceResponse = ObjectMapper()
                .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream("pikachu_response.json"), JsonNode::class.java)
        val secondServiceResponse = ObjectMapper().readValue("{}", JsonNode::class.java)
        val pokemonName = "pikachu"
        val firstServiceUrl = "$pokemonApiUrl/$pokemonName"
        val secondServiceUrl = "https://fake/api/v2/pokemon-species/25/"


        `when`(mockRestTemplate?.getForEntity(eq(firstServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(firstServiceResponse, HttpStatus.OK))

        `when`(mockRestTemplate?.getForEntity(eq(secondServiceUrl), eq(JsonNode::class.java)))
                .thenReturn(generateMockResponseForPokemonService(secondServiceResponse, HttpStatus.OK))

        val response = pokemonService.getPokemonDescriptionByName(pokemonName).get()
        Assert.assertTrue(response == null)

    }

}