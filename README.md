# Shakespearean Pokemon API #

### Build ###

* Make sure to have Java 11 and Maven installed
* Run the following command to build the artifact:
> mvn clean package spring-boot:repackage 

### Running ###

* APIs will be listening to the 8080 port, please make sure that no other application is using it
* Run the following command, after building the artifact: 
> java -jar target/shakespearean-pokemon-api-1.0.0.jar
* You will be able now to hit the localhost:8080/pokemon/{pokemonName} endpoint



### Build Docker image ###
* Make sure to have Docker installed
* Run the following command to build the Docker image:
> docker build . --tag shakespearean-pokemon-api:1.0.0

### Run Docker image ###
* Run the following command to run the Docker image:
> docker run -it -p 127.0.0.1:8080:8080 shakespearean-pokemon-api:1.0.0
* You will be able now to hit the localhost:8080/pokemon/{pokemonName} endpoint

### Future Improvements ###
* exposing metrics through jmx_exporter in order to be read by Prometheus
* adding CI/CD
* use circuitbreakers to call http services
* better error handling about shakespeare translate api (rate limit)
* understand if there is way to avoid directly mocking external api with mockito... 
Maybe it should be better to spawn a mocked server with testContainers for integration tests
